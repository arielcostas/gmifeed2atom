# Conversor de feed gemini feed a Atom

Convierte feeds Gemini (enlaces con fecha) a feed Atom, que pueden ser modificados para que apunten al proxy, facilitando las suscripciones por HTTPS.

## ¿Por qué?

El [protocolo Gemini](https://gemini.circumlunar.space/) implementa su propio sistema de sindicación de contenido, además de los feed Atom y RSS. Muchos usuarios del geminiespacio eligen utilizar este sistema en lugar de mantener un feed XML, por su sencillez.

Sin embargo, esto complica a aquellos que no siempre disponemos de clientes Gemini o queremos usar un agregador de feeds web como Miniflux. Por eso, esta herramienta permite generar un feed Atom a partir de entradas gemini con el siguiente formato:

```gemini
=> /path/to/resource.gmi 2022-04-16 Una página
```

Para dicha página se genera un feed Atom, donde cada enlace de esos añade un `<item>` al feed, con el título, fecha y enlace correctos. También permite gestionar suscripciones para archivos multimedia, como imágenes y vídeos, o podcast.

## Inspiración

La creación de este programa fue inspirada por [C3PO](https://proxy.aljadra.xyz), que comenzó un "podcast" en Gemini y no mantenía feed Atom. Al no poder disponer siempre de navegador Gemini, escribí esta herramienta para poder suscribirme desde mi agregador de feeds.

Al poco tiempo comencé a utilizar el "podcatcher" [AntennaPod](https://antennapod.org/) para mantener todos mis podcast en un mismo lugar. Sin embargo, se presentó un problema con esta herramienta: no detectaba que el feed fuera un podcast. Tras los cambios correspondientes, ahora se detecta correctamente a través de la extensión.

## Uso

Para compilar basta con ejecutar `go build .`, requiriendo al menos Go 1.15 por compatibilidad con la biblioteca que gestiona la conexión por Gemini. Una vez compilado, al ejecutarlo sin pasarle opciones, o pasando `-h` mostrará la ayuda.

El programa escribe por defecto a stdout, con lo que para guardarlo a un archivo hay que redirigirlo. Además, utiliza las URL para el protocolo Gemini, con lo que hay que convertirlas a la URL de un proxy.

Por ejemplo, para suscribirse a `gemini://foobar.tld/gemlog.gmi` podría utilizarse el siguiente comando:

```bash
./gmifeed2atom -author "Foo Bar" -title "Gemlog de Foo Bar" \
	-url "gemini://foo.bar/gemlog.gmi" | sed 's|gemini://|https://proxy.' > gemlog-foobar.xml
```

## Licencias

Este programa es libre, cualquiera puede utilizarlo para cualquier fin y de cualquier modo, compartirlo y modificarlo con arreglo a la licencia BSD 3-Clause, incluída en el archivo `LICENSE` de este repositorio.
