// SPDX-License-Identifier: BSD-3-Clause
package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"mime"
	"os"
	"regexp"
	"strings"
	"text/template"
	"time"

	"git.sr.ht/~adnano/go-gemini"
)

type FeedEntry struct {
	Url   string
	Title string
	Date  string
	Media bool
}

type FeedParams struct {
	Title   string
	Url     string
	Now     string
	Logo    string
	Author  string
	Entries []FeedEntry
}

func main() {
	url := flag.String("url", "", "Archivo para el que generar suscripciones. Por ejemplo `gemini://foo.bar/gemlog.gmi`.")
	title := flag.String("title", "Mi feed Atom para una cápsula Gemini", "Título a utilizar en el feed.")
	author := flag.String("author", "Pepito", "El nombre del autor de los artículos")
	logo := flag.String("logo", "", "Una URL gemini apuntando a una imagen para usar de logo en el feed")
	help := flag.Bool("h", false, "Muestra esta ayuda")
	flag.Parse()

	if *help {
		flag.Usage()
		os.Exit(0)
	}

	if *url == "" {
		flag.Usage()
		os.Exit(1)
	}

	client := gemini.Client{}
	req, err := gemini.NewRequest(*url)
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	if err != nil {
		panic(err)
	}

	resp, err := client.Do(ctx, req)
	if err != nil {
		panic(err)
	}

	links := make([]gemini.LineLink, 0)

	gemini.ParseLines(resp.Body, func(l gemini.Line) {
		if h, ok := l.(gemini.LineLink); ok {
			links = append(links, h)
		}
	})

	var domainExp = regexp.MustCompile(`^(?:gemini:\/\/)?(?:[^@\/\n]+@)?([^:\/\n]+)`)
	var domain = domainExp.FindString(*url)

	var entries = make([]FeedEntry, 0)
	var linkRegexp = regexp.MustCompile(`^\d{4}-\d{2}-\d{2}`)
	var extensionRegexp = regexp.MustCompile(`\.(.*)$`)
	for _, link := range links {
		if !linkRegexp.MatchString(link.Name) {
			continue
		}

		var tm, err = time.Parse("2006-01-02", link.Name[0:10])
		if err != nil {
			println("Error parsing date: ", err.Error())
			continue
		}

		var fe = FeedEntry{
			Title: strings.TrimSpace(link.Name[10:]),
			Url:   domain + link.URL,
			Date:  tm.Format(time.RFC3339),
		}

		/* If extension is some kind of multimedia, make it so in the entry */
		var ext = extensionRegexp.FindString(link.URL)
		if err != nil {
			println("Error obtaning extension: ", err.Error())
			continue
		}
		var mimetype = mime.TypeByExtension(ext)
		fe.Media = false
		if len(mimetype) > 0 {
			group := strings.Split(mimetype, "/")
			if len(group) > 2 {
				fe.Media = false
			} else {
				fe.Media = group[0] == "video" || group[0] == "audio" || group[0] == "image"
			}
		}

		entries = append(entries, fe)
	}

	temp, err := template.New("").Parse(feedTemplate)
	if err != nil {
		panic(err)
	}

	var result bytes.Buffer
	err = temp.Execute(&result, FeedParams{
		Now:     time.Now().Format(time.RFC3339),
		Title:   strings.TrimSpace(*title),
		Url:     *url,
		Logo:    *logo,
		Author:  *author,
		Entries: entries,
	})
	if err != nil {
		panic(err)
	}

	fmt.Println(result.String())
}

var feedTemplate string = `<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
	<id>{{ .Url }}</id>
	<title>{{ .Title }}</title>
	<link href="{{ .Url }}"/>
	<updated>{{ .Now }}</updated>
	<author>
		<name>{{ .Author }}</name>
	</author>
	<generator uri="https://gitlab.com/arielcostas/gmifeed2atom">
		gmifeed2atom
	</generator>
	{{ if ne .Logo "" }}
	<logo>{{ .Logo }}</logo>
	{{ end }}
	{{ range .Entries -}}
	<entry>
		<id>{{ .Url }}</id>
		<title>{{ .Title }}</title>
		<link href="{{ .Url }}" {{ if .Media}}rel="enclosure"{{end}}/>
		<updated>{{ .Date }}</updated>
	</entry>
	{{ end }}
</feed>
`
